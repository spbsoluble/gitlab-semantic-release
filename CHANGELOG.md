## [1.1.3](https://gitlab.com/spbsoluble/gitlab-semantic-release/compare/1.1.2...1.1.3) (2022-11-15)


### Bug Fixes

* **ci:** Don't print environment out ([4c8e6d3](https://gitlab.com/spbsoluble/gitlab-semantic-release/commit/4c8e6d350b85be3dbdb52134b036f83ded5fdab3))

## [1.1.2](https://gitlab.com/spbsoluble/gitlab-semantic-release/compare/1.1.1...1.1.2) (2022-11-15)


### Bug Fixes

* **ci:** Add token username ([8cb9f59](https://gitlab.com/spbsoluble/gitlab-semantic-release/commit/8cb9f59da48da9de03e65b77caa432785071af42))
* **ci:** Release to use self image ([b4a52e2](https://gitlab.com/spbsoluble/gitlab-semantic-release/commit/b4a52e2547f91fd346bbe513ee4e0cc5704396c4))
* **ci:** Remove docker pull ([95f4b3d](https://gitlab.com/spbsoluble/gitlab-semantic-release/commit/95f4b3d54fda401aa51700e2d98f9ae3a41464cf))
* **ci:** Revert token ([f92d585](https://gitlab.com/spbsoluble/gitlab-semantic-release/commit/f92d585d94cc2e269ee5b7694eb97c5197721917))
* **ci:** Update CI to not be user specific. ([0ed27e4](https://gitlab.com/spbsoluble/gitlab-semantic-release/commit/0ed27e49652777c5ffec06590ca29afed9623956))
* **ci:** Update token to job token. ([7b8b800](https://gitlab.com/spbsoluble/gitlab-semantic-release/commit/7b8b8001163c0fb2fd66b8737162af91f21e62d9))

### [1.1.1](https://gitlab.com/ujlbu4/gitlab-semantic-release/compare/1.1.0...1.1.1) (2020-08-28)


### Bug Fixes

* disable success/fail jobs ([298fb05](https://gitlab.com/ujlbu4/gitlab-semantic-release/commit/298fb055b6b2729fa8b263e51939e66261098be2))

## [1.1.0](https://gitlab.com/ujlbu4/gitlab-semantic-release/compare/1.0.0...1.1.0) (2020-08-28)


### Features

* add notifications ([92da5cd](https://gitlab.com/ujlbu4/gitlab-semantic-release/commit/92da5cdf3efaf6189bd90d5f30370ca2fb5b44d2))

## [1.0.0](https://gitlab.com/ujlbu4/gitlab-semantic-release/compare/...1.0.0) (2020-08-28)


### Bug Fixes

* fake release (test release) ([e8599cb](https://gitlab.com/ujlbu4/gitlab-semantic-release/commit/e8599cbed61aa5cf289a75fed710ce3d0ddff0f1))
